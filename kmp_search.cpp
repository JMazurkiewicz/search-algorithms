#include <numeric>
#include "kmp_search.h"

namespace {

std::vector<int> make_partial_match_table(const std::string& pattern) {

    std::vector<int> table(pattern.size() + 1);
    table[0] = -1;

    int table_pos = 1;
    int pattern_pos = 0;

    while(table_pos < static_cast<int>(pattern.size())) {

        if(pattern[table_pos] == pattern[pattern_pos]) {
            table[table_pos] = table[pattern_pos];
        } else {
            
            table[table_pos] = pattern_pos;
            pattern_pos = table[pattern_pos];
            
            while(pattern_pos >= 0 && pattern[table_pos] != pattern[pattern_pos]) {
                pattern_pos = table[pattern_pos];
            }

        }

        ++table_pos;
        ++pattern_pos;

    }

    table[table_pos] = pattern_pos;
    return table;

}

std::vector<int> do_kmp_search(const std::string& pattern, const std::string& text) {

    const std::vector<int> table = make_partial_match_table(pattern);
    std::vector<int> result;

    int text_pos = 0;
    int pattern_pos = 0;

    while(text_pos < static_cast<int>(text.size())) {

        if(pattern[pattern_pos] == text[text_pos]) {

            ++text_pos;
            ++pattern_pos;

            if(pattern_pos == static_cast<int>(pattern.size())) {
                result.push_back(text_pos - pattern_pos);
                pattern_pos = table[pattern_pos];
            }

        } else {

            pattern_pos = table[pattern_pos];
            if(pattern_pos < 0) {
                ++text_pos;
                ++pattern_pos;
            }

        }

    }

    return result;

}

}

std::vector<int> kmp_search(const std::string& pattern, const std::string& text) {

    if(text.empty() || pattern.size() > text.size()) {
        return {};
    }

    if(pattern.empty()) {
        std::vector<int> result(text.size());
        std::iota(result.begin(), result.end(), 0);
        return result;
    }

    return do_kmp_search(pattern, text);

}