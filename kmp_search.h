#pragma once

#include <string>
#include <vector>

std::vector<int> kmp_search(const std::string& pattern, const std::string& text);