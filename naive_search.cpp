#include "naive_search.h"
#include <numeric>

std::vector<int> naive_search(const std::string& pattern, const std::string& text) {

    const std::size_t pattern_length = pattern.size();
    const std::size_t text_length = text.size();

    if(text_length == 0 || pattern_length > text_length) {
        return {};
    }

    if(pattern.empty()) {
        std::vector<int> result(text.size());
        std::iota(result.begin(), result.end(), 0);
        return result;
    }

    std::vector<int> result;

    for(std::size_t text_pos = 0; text_pos <= text_length-pattern_length; ++text_pos) {

        std::size_t pattern_pos = 0;

        while(pattern_pos < pattern_length && text[text_pos+pattern_pos] == pattern[pattern_pos]) {
            ++pattern_pos;
        }

        if(pattern_pos == pattern_length){
            result.push_back(text_pos);
        }
    
    }

    return result;

}