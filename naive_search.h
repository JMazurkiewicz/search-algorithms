#pragma once

#include <string>
#include <vector>

std::vector<int> naive_search(const std::string& pattern, const std::string& text);