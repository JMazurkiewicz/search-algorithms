#include <iostream>
#include "kmp_search.h"
#include "naive_search.h"
#include <string_view>
#include "test_utility.h"

class basic_search_test {

public:

    using search_result = std::vector<int>;
    using search_function = search_result(&)(const std::string&, const std::string&);

private:

    search_function algorithm;
    std::string algorithm_name;

public:

    explicit basic_search_test(search_function algorithm, std::string_view algorithm_name):
        algorithm(algorithm), algorithm_name(algorithm_name) { }

    basic_search_test(const basic_search_test&) = delete;
    basic_search_test& operator=(const basic_search_test&) = delete;

    void run_tests() {

        intro();

        empty_text_empty_pattern_test();
        empty_text_test();
        empty_pattern_test();
        pattern_larger_than_text_test();
        pattern_equal_to_text_test();
        no_pattern_in_text_test();
        overlapping_pattern_in_text_test();
        pattern_at_the_end_of_text_test();

    }

private:

    void intro() {
        std::cout << "Testing " << algorithm_name << " algorithm for specific cases:\n";
    }

    void empty_text_empty_pattern_test() {
        const search_result result = algorithm("", "");
        test(result, search_result{}, __func__);
    }

    void empty_text_test() {
        const search_result result = algorithm("AAAAA", "");
        test(result, search_result{}, __func__);
    }

    void empty_pattern_test() {
        const search_result result = algorithm("", "BBBBB");
        test(result, search_result{0, 1, 2, 3, 4}, __func__);
    }

    void pattern_larger_than_text_test() {
        const search_result result = algorithm("CCCCCCCC", "CCCC");
        test(result, search_result{}, __func__);
    }

    void pattern_equal_to_text_test() {
        
        const std::string str = "DDDDDDDD";
        const search_result result = algorithm(str, str);

        test(result, search_result{0}, __func__);

    }

    void no_pattern_in_text_test() {
        const search_result result = algorithm("F", "EEEEEEEE");
        test(result, search_result{}, __func__);
    }

    void overlapping_pattern_in_text_test() {
        const search_result result = algorithm("AABAA", "AABAABAABAABAA");
        test(result, search_result{0, 3, 6, 9}, __func__);
    }

    void pattern_at_the_end_of_text_test() {
        const search_result result = algorithm("AA", "BBBBAAA");
        test(result, search_result{4, 5}, __func__);
    }

    // @todo replace std::string_view parameter with C++20 std::source_location default parameter
    void test(const search_result& result, const search_result& expected, std::string_view test_name) {

        std::cout << "  \"" << test_name << "\": ";

        if(result == expected) {
            std::cout << "passed";
        } else {
            fail(result, expected);
        }

        std::cout << std::endl;
        
    }

    void fail(const search_result& result, const search_result& expected) {
        std::cout << "failed\n";
        std::cout << "    expected:\n";
        std::cout << "      " << expected << '\n';
        std::cout << "    got:\n";
        std::cout << "      " << result;
    }

};

int main() {

    basic_search_test naive_search_test(naive_search, "naive");
    naive_search_test.run_tests();

    basic_search_test kmp_search_test(kmp_search, "Knuth-Morris-Pratt");
    kmp_search_test.run_tests();

}