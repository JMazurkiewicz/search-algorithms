#include <algorithm>
#include <iostream>
#include <iterator>
#include "kmp_search.h"
#include "naive_search.h"
#include <random>
#include <string_view>
#include "test_utility.h"

class search_comparison {

private:

    std::string text;
    std::string pattern;

    static constexpr std::string::size_type text_size = 100;
    static constexpr std::string::size_type pattern_size = 4;

public:

    using search_result = std::vector<int>;

    template<typename Generator>
    explicit search_comparison(Generator& generator) {
        generate_data(generator);
    }

    search_comparison(const search_comparison&) = delete;
    search_comparison& operator=(const search_comparison&) = delete;

    void run_test() {

        intro();

        const search_result kmp_result = kmp_search(pattern, text);
        print_result(kmp_result, "Knuth-Morris-Pratt");

        const search_result naive_result = naive_search(pattern, text);
        print_result(naive_result, "naive");

        compare_results(kmp_result, naive_result);

    }

private:

    template<typename Generator>
    void generate_data(Generator& generator) {

        std::uniform_int_distribution<int> distribution('A', 'B');
        auto random_char = [&generator, &distribution]() -> char {
            return distribution(generator);
        };

        text.reserve(text_size);
        std::generate_n(std::back_inserter(text), text_size, random_char);

        pattern.reserve(pattern_size);
        std::generate_n(std::back_inserter(pattern), pattern_size, random_char);

    }

    void intro() {
        std::cout << "Comparison: Knuth-Morris-Pratt and naive:\n";
        std::cout << "  text: " << text << '\n';
        std::cout << "  pattern: " << pattern << '\n';
    }

    void print_result(const search_result& result, std::string_view algorithm_name) {

        std::cout << "  " << algorithm_name << " result:\n";
        std::cout << "    " << result << '\n';
        std::cout << "    " << text << '\n';
        std::cout << "    " << std::right;
        
        int position = 0;

        for(const int match_position : result) {
        
            const std::streamsize space_width = std::max(0, match_position - position);
            std::fill_n(std::ostream_iterator<char>{std::cout}, space_width, ' ');

            const std::streamsize match_width = pattern.size() - std::max(0, position - match_position);
            std::fill_n(std::ostream_iterator<char>{std::cout}, match_width, '~');

            position += match_width + space_width;

        }

        std::cout << '\n';

    }

    void compare_results(const search_result& kmp_result, const search_result& naive_result) {

        if(kmp_result == naive_result) {
            std::cout << "  Knuth-Morris-Pratt result == naive result\n";
        } else {
            std::cout << "  results are different, details:\n";
            search_with(kmp_result, "Knuth-Morris-Pratt");
            search_with(naive_result, "naive");
        }

    }

    void search_with(const search_result& result, std::string_view algorithm_name) {

        std::cout << "    " << algorithm_name << " algorithm:\n";

        for(int position : result) {
            std::cout << "      found at: " << position << " -> ";
            const std::string found = text.substr(position, pattern.size());
            test(found, pattern);
        }

    }

    void test(const std::string& found, const std::string& expected) {

        if(found == expected) {
            std::cout << "success";
        } else {
            std::cout << "fail\n";
            fail(found, expected);
        }

        std::cout << std::endl;

    }

    void fail(const std::string& found, const std::string& expected) {
        std::cout << "        expected:\n";
        std::cout << "          " << expected << '\n';
        std::cout << "        got:\n";
        std::cout << "          " << found;
    }

};

int main() {

    std::mt19937 generator{0x738AED99};
    search_comparison comparison{generator};
    comparison.run_test();

}