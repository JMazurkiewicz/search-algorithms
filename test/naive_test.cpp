#include <fstream>
#include <iostream>
#include <sstream>
#include "naive_search.h"
#include <vector>

std::string get_file_contents(const char* path) {

    std::ostringstream sstream;
    std::ifstream file(path);

    sstream << file.rdbuf();
    return sstream.str();

}

class naive_search_test {

private:

    std::string text;
    std::vector<std::string> patterns;

public:

    explicit naive_search_test(const std::string& text, std::initializer_list<std::string> patterns):
        text{text}, patterns{patterns} { }

    naive_search_test(const naive_search_test&) = delete;
    naive_search_test& operator=(const naive_search_test&) = delete;

    void run_test() {

        intro();

        for(const std::string& pattern : patterns) {
            std::cout << "  searching for \"" << pattern << "\" in text:\n";
            search_for(pattern);
        }

    }

private:

    void intro() {
        std::cout << "Testing naive algorithm:\n";
    }

    void search_for(const std::string& pattern) {

        for(int position : naive_search(pattern, text)) {

            std::cout << "    found at: " << position << " -> ";
            const std::string found = text.substr(position, pattern.size());
            test(found, pattern);

        }

    }

    void test(const std::string& found, const std::string& expected) {

        if(found == expected) {
            std::cout << "success";
        } else {
            std::cout << "fail\n";
            fail(found, expected);
        }

        std::cout << std::endl;

    }

    void fail(const std::string& found, const std::string& expected) {
        std::cout << "      expected:\n";
        std::cout << "        " << expected << '\n';
        std::cout << "      got:\n";
        std::cout << "        " << found;
    }

};

int main() {

    using namespace std::literals;

    naive_search_test test{get_file_contents("lorem-ipsum.txt"), {
        "ipsum"s, "mi"s, "sit"s, "vel"s, "id"s
    }};
    test.run_test();

}