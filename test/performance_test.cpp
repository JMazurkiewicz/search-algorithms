#include <chrono>
#include <fstream>
#include <iostream>
#include <iterator>
#include "kmp_search.h"
#include "naive_search.h"
#include <random>

class performance_test {

public:

    using search_function = std::vector<int>(&)(const std::string&, const std::string&);
    using duration = std::chrono::duration<float, std::micro>;

private:

    struct performance_test_result {
        std::size_t text_size;
        duration kmp_performance;
        duration naive_performance;
    };

    std::string text;
    std::string pattern;
    std::vector<performance_test_result> results;

public:

    template<typename Generator>
    explicit performance_test(Generator& generator, std::size_t text_size, std::size_t pattern_size) {

        std::uniform_int_distribution<int> distribution('A', 'B');
        auto random_char = [&generator, &distribution]() -> char {
            return distribution(generator);
        };
        
        text.reserve(text_size);
        std::generate_n(std::back_inserter(text), text_size, random_char);

        pattern.reserve(pattern_size);
        std::generate_n(std::back_inserter(pattern), pattern_size, random_char);

    }

    void run_test() {

        std::string current_text;
        for(const char c : text) {

            current_text += c;
            results.push_back({
                current_text.size(),
                measure_execution_duration(kmp_search, current_text),
                measure_execution_duration(naive_search, current_text)
            });

        }

    }

private:

    duration measure_execution_duration(search_function algorithm, const std::string& text) {

        const auto start = std::chrono::high_resolution_clock::now();
        algorithm(pattern, text);
        const auto stop = std::chrono::high_resolution_clock::now();

        return std::chrono::duration_cast<duration>(stop - start);

    }

    friend std::ostream& operator<<(std::ostream& ostream, const performance_test& test) {

        ostream << "\tkmp_search\tnaive_search\n";
        for(auto&& result : test.results) {
            ostream << result.text_size << '\t';
            ostream << result.kmp_performance.count() << '\t';
            ostream << result.naive_performance.count() << '\n';
        }

        return ostream;

    }

};

int main() {

    std::mt19937 generator{0xDEADBEEF};

    performance_test test{generator, 1'000, 5};
    test.run_test();

    struct xlsx_punct : std::numpunct<char> {
        virtual char_type do_decimal_point() const override {
            return ',';
        }
    };

    std::ofstream file{"performance-test-result.txt"};
    file.imbue(std::locale{file.getloc(), new xlsx_punct{}});
    file << test;

}