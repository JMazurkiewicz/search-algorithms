import os, sys, platform, getopt

def wait_for_enter():
    input("\nPress enter to continue...")

EXECUTEABLE_EXTENSION = ".exe" if platform.system() == "Windows" else ".out"

class cpp_test:
    
    def __init__(self, options, test_list):
        self.__options = options
        self.__test_list = []
        self.make_test_list(test_list)

    def make_test_list(self, test_list):
        if not test_list:
            for file_name in os.listdir("."):
                if file_name.endswith(".cpp"):
                    self.__test_list.append(file_name)
        else:
            self.__test_list = test_list

    def run(self):
        if ("--build", "") in self.__options:
            self.build_tests()
        if ("--notest", "") not in self.__options:
            self.run_tests()
        if ("--remove", "") in self.__options:
            self.remove_executeables()

    def build_tests(self):
        for test_name in self.__test_list:
            self.build_test(test_name)

    def build_test(self, file_name):
        executeable_name = self.make_executeable_name(file_name)
        compiler_name = "gcc" if ("--gcc", "") in self.__options else "clang"
        result = os.system(
            compiler_name + " -std=c++17 -Wall --include-directory .. " + file_name +
            " -o " + executeable_name + " ../naive_search.cpp ../kmp_search.cpp"
        )
        if result != 0:
            wait_for_enter()
            exit()

    def make_executeable_name(self, test_name):
        return os.path.splitext(test_name)[0] + EXECUTEABLE_EXTENSION

    def run_tests(self):
        for test_name in self.__test_list:
            self.run_test(self.make_executeable_name(test_name))
        wait_for_enter()

    def run_test(self, test_name):
        command = os.path.join(".", test_name)
        os.system(command)

    def remove_executeables(self):
        for test_name in self.__test_list:
            os.remove(self.make_executeable_name(test_name))

def main():

    try:
        options, test_list = getopt.getopt(sys.argv[1:], "", ["build", "notest", "remove", "gcc"])
    except getopt.error as e:
        print("Error: " + e)
        exit()

    test = cpp_test(options, test_list)
    test.run()

if __name__ == "__main__":
    main()