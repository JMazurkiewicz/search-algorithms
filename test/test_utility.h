#pragma once

#include <ostream>
#include <vector>

inline std::ostream& operator<<(std::ostream& stream, const std::vector<int>& result) {

    stream << '[';

    if(auto it = result.begin(); it != result.end()) {
        stream << *it;
        while(++it != result.end()) {
            stream << ", " << *it;
        }
    }

    return stream << ']';

}